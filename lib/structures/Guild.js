"use strict";

const Base = require("./Base");
const Channel = require("./Channel");
const Collection = require("../util/Collection");
const GuildChannel = require("./GuildChannel");
const Member = require("./Member");
const VoiceState = require("./VoiceState");

/**
* Represents a guild
* @prop {String?} afkChannelID The ID of the AFK voice channel
* @prop {Number} afkTimeout The AFK timeout in seconds
* @prop {String?} applicationID The application ID of the guild creator if it is bot-created
* @prop {Number?} approximateMemberCount The approximate number of members in the guild (REST only)
* @prop {Number?} approximatePresenceCount The approximate number of presences in the guild (REST only)
* @prop {Boolean?} autoRemoved Whether the guild was automatically removed from Discovery
* @prop {String?} banner The hash of the guild banner image, or null if no banner (VIP only)
* @prop {String?} bannerURL The URL of the guild's banner image
* @prop {Array<Object>?} categories The guild's discovery categories
* @prop {Collection<GuildChannel>} channels Collection of Channels in the guild
* @prop {Number} createdAt Timestamp of the guild's creation
* @prop {Number} defaultNotifications The default notification settings for the guild. 0 is "All Messages", 1 is "Only @mentions"
* @prop {String?} description The description for the guild (VIP only)
* @prop {String?} discoverySplash The has of the guild discovery splash image, or null if no discovery splash
* @prop {String?} discoverySplashURL The URL of the guild's discovery splash image
* @prop {Number?} emojiCount The number of emojis on the guild
* @prop {Array<Object>} emojis An array of guild emoji objects
* @prop {Number} explicitContentFilter The explicit content filter level for the guild. 0 is off, 1 is on for people without roles, 2 is on for all
* @prop {Array<String>} features An array of guild feature strings
* @prop {String?} icon The hash of the guild icon, or null if no icon
* @prop {String?} iconURL The URL of the guild's icon
* @prop {String} id The ID of the guild
* @prop {Number} joinedAt Timestamp of when the bot account joined the guild
* @prop {Array<String>?} keywords The guild's discovery keywords
* @prop {Boolean} large Whether the guild is "large" by "some Discord standard"
* @prop {Number} mfaLevel The admin 2FA level for the guild. 0 is not required, 1 is required
* @prop {Number} maxMembers The maximum amount of members for the guild
* @prop {Number} maxPresences The maximum number of people that can be online in a guild at once (returned from REST API only)
* @prop {Number?} maxVideoChannelUsers The max number of users allowed in a video channel
* @prop {Number} memberCount Number of members in the guild
* @prop {Collection<Member>} members Collection of Members in the guild
* @prop {String} name The name of the guild
* @prop {String} ownerID The ID of the user that is the guild owner
* @prop {String} preferredLocale Preferred "COMMUNITY" guild language used in server discovery and notices from Discord
* @prop {Number?} premiumSubscriptionCount The total number of users currently boosting this guild
* @prop {Number} premiumTier Nitro boost level of the guild
* @prop {Object?} primaryCategory The guild's primary discovery category
* @prop {Number?} primaryCategoryID The guild's primary discovery category ID
* @prop {String?} publicUpdatesChannelID ID of the guild's updates channel if the guild has "COMMUNITY" features
* @prop {String} region The region of the guild
* @prop {Collection<Role>} roles Collection of Roles in the guild
* @prop {String?} rulesChannelID The channel where "COMMUNITY" guilds display rules and/or guidelines
* @prop {Shard} shard The Shard that owns the guild
* @prop {String?} splash The hash of the guild splash image, or null if no splash (VIP only)
* @prop {String?} splashURL The URL of the guild's splash image
* @prop {Number} systemChannelFlags The flags for the system channel
* @prop {String?} systemChannelID The ID of the default channel for system messages (built-in join messages and boost messages)
* @prop {Boolean} unavailable Whether the guild is unavailable or not
* @prop {String?} vanityURL The vanity URL of the guild (VIP only)
* @prop {Number} verificationLevel The guild verification level
* @prop {Collection<VoiceState>} voiceStates Collection of voice states in the guild
* @prop {Object?} welcomeScreen The welcome screen of a Community guild, shown to new members
* @prop {Object} welcomeScreen.description The description in the welcome screen
* @prop {Array<Object>} welcomeScreen.welcomeChannels The list of channels in the welcome screens. Each channels have the following properties: `channelID`, `description`, `emojiID`, `emojiName`. `emojiID` and `emojiName` properties can be null.
* @prop {Number?} widgetChannelID The channel id that the widget will generate an invite to. REST only.
* @prop {Boolean?} widgetEnabled Whether the guild widget is enabled. REST only.
*/
class Guild extends Base {
    constructor(data, client) {
        super(data.id);
        this._client = client;
        this.shard = client.shards.get(client.guildShardMap[this.id]);
        this.unavailable = !!data.unavailable;
        this.joinedAt = Date.parse(data.joined_at);
        this.voiceStates = new Collection(VoiceState);
        this.channels = new Collection(GuildChannel);
        this.members = new Collection(Member);
        this.memberCount = data.member_count;
        this.applicationID = data.application_id;

        if(data.channels) {
            for(const channelData of data.channels) {
                channelData.guild_id = this.id;
                const channel = Channel.from(channelData, client);
                channel.guild = this;
                this.channels.add(channel, client);
                client.channelGuildMap[channel.id] = this.id;
            }
        }

        if(data.members) {
            for(const member of data.members) {
                member.id = member.user.id;
                this.members.add(member, this);
            }
        }

        if(data.presences) {
            for(const presence of data.presences) {
                if(!this.members.get(presence.user.id)) {
                    let userData = client.users.get(presence.user.id);
                    if(userData) {
                        userData = `{username: ${userData.username}, id: ${userData.id}, discriminator: ${userData.discriminator}}`;
                    }
                    client.emit("debug", `Presence without member. ${presence.user.id}. In global user cache: ${userData}. ` + JSON.stringify(presence), this.shard.id);
                    continue;
                }
                presence.id = presence.user.id;
                this.members.update(presence);
            }
        }

        if(data.voice_states) {
            if(!client.bot) {
                this.pendingVoiceStates = data.voice_states;
            } else {
                for(const voiceState of data.voice_states) {
                    if(!this.members.get(voiceState.user_id)) {
                        continue;
                    }
                    voiceState.id = voiceState.user_id;
                    try {
                        const channel = this.channels.get(voiceState.channel_id);
                        const member = this.members.update(voiceState);
                        if(channel && channel.voiceMembers) {
                            channel.voiceMembers.add(member);
                        }
                    } catch(err) {
                        client.emit("error", err, this.shard.id);
                        continue;
                    }
                    if(client.options.seedVoiceConnections && voiceState.id === client.user.id && !client.voiceConnections.get(this.id)) {
                        process.nextTick(() => this._client.joinVoiceChannel(voiceState.channel_id));
                    }
                }
            }
        }
    }

    /**
    * Kick a member from the guild
    * @arg {String} userID The ID of the member
    * @arg {String} [reason] The reason to be displayed in audit logs
    * @returns {Promise}
    */
    kickMember(userID, reason) {
        return this._client.kickGuildMember.call(this._client, this.id, userID, reason);
    }

    /**
    * Leave the guild
    * @returns {Promise}
    */
    leave() {
        return this._client.leaveGuild.call(this._client, this.id);
    }

    toJSON(props = []) {
        return super.toJSON([
            "afkChannelID",
            "afkTimeout",
            "applicationID",
            "approximateMemberCount",
            "approximatePresenceCount",
            "autoRemoved",
            "banner",
            "categories",
            "channels",
            "defaultNotifications",
            "description",
            "discoverySplash",
            "emojiCount",
            "emojis",
            "explicitContentFilter",
            "features",
            "icon",
            "joinedAt",
            "keywords",
            "large",
            "maxMembers",
            "maxPresences",
            "maxVideoChannelUsers",
            "memberCount",
            "members",
            "mfaLevel",
            "name",
            "ownerID",
            "pendingVoiceStates",
            "preferredLocale",
            "premiumSubscriptionCount",
            "premiumTier",
            "primaryCategory",
            "primaryCategoryID",
            "publicUpdatesChannelID",
            "region",
            "rulesChannelID",
            "splash",
            "systemChannelFlags",
            "systemChannelID",
            "unavailable",
            "vanityURL",
            "verificationLevel",
            "voiceStates",
            "welcomeScreen",
            "widgetChannelID",
            "widgetEnabled",
            ...props
        ]);
    }
}

module.exports = Guild;
