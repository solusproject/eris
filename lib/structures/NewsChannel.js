"use strict";

const TextChannel = require("./TextChannel");

/**
* Represents a guild news channel. See TextChannel for more properties and methods.
* @extends TextChannel
*/
class NewsChannel extends TextChannel {

}

module.exports = NewsChannel;
