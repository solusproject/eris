# Eris Fork
This is a fork of the Discord API Library Eris.
It is protected by the MIT license by its original authors, and since we have only removed features to increase efficiency, we intend to keep its original license.

## Features
As of right now, sharding features remain intact. You can kick/ban users, read messages, get raw message data, and that's about it. Channel/Server management features, Voice features, and such have been removed.
The near-bare minimum for Sophie's operation is still intact. 

## Performance
We will test how fast this library runs with Sophie shortly, with a comparison between unmodified Discord.js and modified Eris.

## Installation
If you wish to install this library, you will have to copy/paste the library files into your node_modules folder.
We will not publish to NPM and this library is going to only be used with SophieBot on Discord.